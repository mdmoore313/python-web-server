﻿#Project 2 - Michael Moore
# October 20, 2015
#Results - Multithreading works, images only load partially :-(

#Declarations
from socket import *
import re
import os
import sys
import thread
import time

def serviceWebReq(clientSocket, address ):
	
	#Grab incoming message
	incMessage = clientSocket.recv(4096)
	print "incMessage: " + incMessage
	
	#Quit if there is not a valid request
	if(not incMessage):
		quit()
	
	#Get the filename the client requested
	clientReq = incMessage.split('\n', 1)[0] 
	
	#Grab filename
	filename = clientReq.split(' ')[1].strip('/')
	print "filename: " + filename
	
	
	#Check for keep alive
	strKeepAlive = re.match('[k|K]eep-Alive.*', incMessage) #Return entire keep alive string if it exists
	
	if(strKeepAlive):
		strTimeout=re.split('[k|K]eep-Alive.*timeout=', strKeepAlive)
		timeout=int(re.split('\s|,|\\r\\n', strTimeout)[0])
		print "timeout: " + str(timeout)
		
	else:
		timeout=0
		
	#Check for standard webpage request, index.html, or helloword.html
	if clientReq.startswith(("GET / ", "GET /index.html", "GET /helloworld.html", "GET helloworld.html")):
	
		filename="helloworld.html"
		print "filename: " + filename

	#end if

	try:

		if os.path.isfile(filename):
			
			#Open our default html file
			outputdata=open(filename)
		
			contentType="text/html"
			
			#Check image format and change content type accordingly
			if(filename.endswith(('jpg','jpeg'))):#,'gif','png','bmp'))):
				contentType="image/jpeg"
			elif(filename.endswith(('gif'))):
				contentType="image/gif"
			elif(filename.endswith(('png'))):
				contentType="image/png"
			elif(filename.endswith(('bmp'))):
				contentType="image/bmp"
			elif(filename.endswith(('tiff'))):
				contentType="image/tiff"
				
			print 'filename: ' + filename +'\r\nContent-Type: ' + contentType + '\r\nContent Length: ' + str(os.path.getsize(filename))
			
			#Send 200 OK response and webpage
			connectionSocket.sendall(	'HTTP/1.1 200 OK\r\n \
						Server: BigHomie\'s Custom Web Server(Window 7)\r\n \
						Last-Modified: Sun, 27 Sep 2015 21:07:02 EST\r\n \
						Accept-Ranges: bytes\r\n \
						Content-Length: '+ str(os.path.getsize(filename)) + '\r\n \
						Content-Type: ' + contentType + ' \r\n\r\n')
			
			#Open the file and send it out byte by byte
			while True:
				data = outputdata.read(100)
				if not data:
					break
				connectionSocket.sendall(data)
				#print data

			#Close connection
			connectionSocket.close()
		
		else:
		#Webpage not found, send 404 page
			outputdata=open("404.html").read()
	
			connectionSocket.sendall('HTTP/1.1 404 NOT FOUND\r\n \
					HTTP/1.1 200 OK\r\n \
					Server: BigHomie\'s Custom Web Server(Window 7)\r\n \
					Last-Modified: Sun, 27 Sep 2015 21:07:02 EST\r\n \
					Accept-Ranges: bytes\r\n \
					Content-Length: '+ str(len(outputdata)) + '\r\n \
					Content-Type: text/html; charset=ISO-8859-1\r\n \
					Keep-Alive: timeout=10, max=100\r\n \
					Connection: keep-alive\r\n\r\n' + outputdata)
	
	#end try
	
	except KeyboardInterrupt:
		#Catch and print exceptions, close socket connections
		print sys.exc_info()[0], sys.exc_info()[1]
		#connectionSocket.close()
		quit()
#end def




#Listening on port 80
serverPort=80 

#Prepare a sever socket
serverSocket = socket(AF_INET, SOCK_STREAM) 

#Bind socket to port
serverSocket.bind(("", serverPort)) 

#Maximum of 1 backlog connections
serverSocket.listen(5) 

connectionSocket = None

#Bind was successful, print msg
print 'Listening on port ' + str(serverPort)

#Now that we're listening, continuously loop while waiting for a message
while True:
	try:
	
		#Accept connection from client
		connectionSocket, addr = serverSocket.accept() 
		
		#Spawn thread to handle request
		thread.start_new_thread(serviceWebReq, (connectionSocket, addr ))
	except: 
		print sys.exc_info()[0], sys.exc_info()[1]
		#connectionSocket.close()
		quit()
serverSocket.close()	